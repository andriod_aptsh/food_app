package com.apantri.food_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.apantri.food_app.adapter.ItemAdapter
import com.apantri.food_app.data.Datasource

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        val myDataset = Datasource().loadAffirmations()
//        val recyclerView = findViewById<RecyclerView>(R.id.recycler1)
//        recyclerView.adapter = ItemAdapter(this,myDataset)
//        recyclerView.setHasFixedSize(true)

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        setupActionBarWithNavController(navController)

    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}

//import androidx.appcompat.app.AppCompatActivity
//import android.os.Bundle
//import androidx.navigation.NavController
//import androidx.navigation.fragment.NavHostFragment
//import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
//import androidx.recyclerview.widget.RecyclerView
//import com.apantri.food_app.adapter.ItemAdapter
//import com.apantri.food_app.data.Datasource
//
//class MainActivity : AppCompatActivity() {
//    private lateinit var navController: NavController
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//        val navHostFragment = supportFragmentManager
//            .findFragmentById(R.id.fragment)as NavHostFragment
//        navController = navHostFragment.navController
//        setupActionBarWithNavController(navController)
//    }
//    override fun onSupportNavigateUp(): Boolean {
//        return navController.navigateUp() || super.onSupportNavigateUp()
//    }
//
////        setContentView(R.layout.fragment_menu1)
////        val myDataset = Datasource().loadAffirmations()
////        val recyclerView = findViewById<RecyclerView>(R.id.recycler1)
////        recyclerView.adapter = ItemAdapter(this,myDataset)
////        recyclerView.setHasFixedSize(true)
//    }
//}