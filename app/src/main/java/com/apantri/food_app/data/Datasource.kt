package com.apantri.food_app.data

import com.apantri.food_app.R
import com.apantri.food_app.model.Affirmation

class Datasource {
    fun loadAffirmations(): List<Affirmation>{
        return listOf<Affirmation>(
            Affirmation(R.string.head_1,R.drawable.menu1_1),
            Affirmation(R.string.head_2,R.drawable.menu1_2),
            Affirmation(R.string.head_3,R.drawable.menu1_3),
            Affirmation(R.string.head_4,R.drawable.menu1_4),
            Affirmation(R.string.head_5,R.drawable.menu1_5),
            Affirmation(R.string.head_6,R.drawable.menu1_6),
            Affirmation(R.string.head_7,R.drawable.menu1_7),
            Affirmation(R.string.head_8,R.drawable.menu1_8),
            Affirmation(R.string.head_9,R.drawable.menu1_9),
            Affirmation(R.string.head_10,R.drawable.menu1_10),
            Affirmation(R.string.head_11,R.drawable.menu1_11),
            Affirmation(R.string.head_12,R.drawable.menu1_12),
            Affirmation(R.string.head_13,R.drawable.menu1_13),
            Affirmation(R.string.head_14,R.drawable.menu1_14),
            Affirmation(R.string.head_15,R.drawable.menu1_15),
            Affirmation(R.string.head_16,R.drawable.menu1_16),
            Affirmation(R.string.head_17,R.drawable.menu1_17),
            Affirmation(R.string.head_18,R.drawable.menu1_18),
            Affirmation(R.string.head_19,R.drawable.menu1_19),
            Affirmation(R.string.head_20,R.drawable.menu1_20)
        )
    }
}