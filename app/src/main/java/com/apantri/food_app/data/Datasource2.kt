package com.apantri.food_app.data

import com.apantri.food_app.R
import com.apantri.food_app.model.Affirmation

class Datasource2 {
    fun loadAffirmations(): List<Affirmation>{
        return listOf<Affirmation>(
            Affirmation(R.string.head_21, R.drawable.inter1),
            Affirmation(R.string.head_22, R.drawable.inter2),
            Affirmation(R.string.head_23, R.drawable.inter3),
            Affirmation(R.string.head_24, R.drawable.inter4),
            Affirmation(R.string.head_25, R.drawable.inter5),
            Affirmation(R.string.head_26, R.drawable.inter6),
            Affirmation(R.string.head_27, R.drawable.inter7),
            Affirmation(R.string.head_28, R.drawable.inter8),
            Affirmation(R.string.head_29, R.drawable.inter9),
            Affirmation(R.string.head_30, R.drawable.inter10),
            Affirmation(R.string.head_31, R.drawable.inter11),
            Affirmation(R.string.head_32, R.drawable.inter12),
            Affirmation(R.string.head_33, R.drawable.inter13),
            Affirmation(R.string.head_34, R.drawable.inter14),
            Affirmation(R.string.head_35, R.drawable.inter15),
            Affirmation(R.string.head_36, R.drawable.inter16),
            Affirmation(R.string.head_37, R.drawable.inter17),
            Affirmation(R.string.head_38, R.drawable.inter18),
            Affirmation(R.string.head_39, R.drawable.inter19),
            Affirmation(R.string.head_40, R.drawable.inter20),

            )
    }
}