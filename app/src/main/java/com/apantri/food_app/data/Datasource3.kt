package com.apantri.food_app.data

import com.apantri.food_app.R
import com.apantri.food_app.model.Affirmation

class Datasource3 {
    fun loadAffirmations(): List<Affirmation>{
        return listOf<Affirmation>(
            Affirmation(R.string.head_41, R.drawable.clean1),
            Affirmation(R.string.head_42, R.drawable.clean2),
            Affirmation(R.string.head_43, R.drawable.clean3),
            Affirmation(R.string.head_44, R.drawable.clean4),
            Affirmation(R.string.head_45, R.drawable.clean5),
            Affirmation(R.string.head_46, R.drawable.clean6),
            Affirmation(R.string.head_47, R.drawable.clean7),
            Affirmation(R.string.head_48, R.drawable.clean8),
            Affirmation(R.string.head_49, R.drawable.clean9),
            Affirmation(R.string.head_50, R.drawable.clean10),
            Affirmation(R.string.head_51, R.drawable.clean11),
            Affirmation(R.string.head_52, R.drawable.clean12),
            Affirmation(R.string.head_53, R.drawable.clean13),
            Affirmation(R.string.head_54, R.drawable.clean14),
            Affirmation(R.string.head_55, R.drawable.clean15),
            Affirmation(R.string.head_56, R.drawable.clean16),
            Affirmation(R.string.head_57, R.drawable.clean17),
            Affirmation(R.string.head_58, R.drawable.clean18),
            Affirmation(R.string.head_59, R.drawable.clean19),
            Affirmation(R.string.head_60, R.drawable.clean20)
            )
    }
}