package com.apantri.food_app

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apantri.food_app.adapter.ItemAdapter
import com.apantri.food_app.data.Datasource
import com.apantri.food_app.databinding.FragmentMenu1Binding

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class MenuFragment1 : Fragment() {

    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentMenu1Binding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMenu1Binding.inflate(inflater,container,false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val myDataset = Datasource().loadAffirmations()
        binding?.recycler1?.adapter = ItemAdapter(this.requireContext(),myDataset)
        binding?.recycler1?.setHasFixedSize(true)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MenuFragment1().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun dataIntialize() {
//        newsArrayList = arrayListOf<News>()
//        imageId = arrayOf(
//            R.drawable.menu1_1,
//            R.drawable.menu1_2,
//            R.drawable.menu1_3,
//            R.drawable.menu1_4,
//            R.drawable.menu1_5,
//            R.drawable.menu1_6,
//            R.drawable.menu1_7,
//            R.drawable.menu1_8,
//            R.drawable.menu1_9,
//            R.drawable.menu1_10,
//            R.drawable.menu1_11,
//            R.drawable.menu1_12,
//            R.drawable.menu1_13,
//            R.drawable.menu1_14,
//            R.drawable.menu1_15,
//            R.drawable.menu1_16,
//            R.drawable.menu1_17,
//            R.drawable.menu1_18,
//            R.drawable.menu1_19,
//            R.drawable.menu1_20,
//        )
//        heading = arrayOf(
//            getString(R.string.head_1),
//            getString(R.string.head_2),
//            getString(R.string.head_3),
//            getString(R.string.head_4),
//            getString(R.string.head_5),
//            getString(R.string.head_6),
//        )
//        news = arrayOf(
//            getString(R.string.news_1),
//            getString(R.string.news_2),
//            getString(R.string.news_3),
//            getString(R.string.news_4),
//            getString(R.string.news_5),
//            getString(R.string.news_6),
//        )
//        for (i in imageId.indices){
//            val news = News(imageId[4], heading[1])
//            newsArrayList.add(news)
//        }
    }

}
