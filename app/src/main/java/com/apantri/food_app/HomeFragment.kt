package com.apantri.food_app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.apantri.food_app.databinding.FragmentHomeBinding

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class HomeFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnMenu1?.setOnClickListener {
            var action = HomeFragmentDirections.actionHomeFragmentToMenuFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnMenu2?.setOnClickListener {
            var action = HomeFragmentDirections.actionHomeFragmentToMenuFragment2()
            view.findNavController().navigate(action)
        }
        binding?.btnMenu3?.setOnClickListener {
            var action = HomeFragmentDirections.actionHomeFragmentToBmiFragment()
            view.findNavController().navigate(action)
        }
//        binding?.btnMenu3?.setOnClickListener {
//            var action = HomeFragmentDirections.actionHomeFragmentToMenuFragment3()
//            view.findNavController().navigate(action)
//        }

    }
}