package com.apantri.food_app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.FragmentFactory
import androidx.navigation.findNavController
import com.apantri.food_app.databinding.FragmentBmiBinding
import com.apantri.food_app.databinding.FragmentMenu3Binding
import kotlin.math.round

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BmiFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BmiFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var shape : String? = "answer"
    private var _binding: FragmentBmiBinding? = null
    private val binding get() = _binding

        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBmiBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.calBtn?.setOnClickListener {
            val num1: Double = view.findViewById<EditText>(R.id.weight_value).text.toString().toDouble()
            val num2: Double = view.findViewById<EditText>(R.id.height_value).text.toString().toDouble()
            val h1: Double = num2 / 100
            val h2: Double = h1 * h1
            val bmi = (String.format("%.2f",(num1 / h2))).toDouble()
            if (bmi < 18){
                shape = "UNDERWEIGHT".toString()
            }else if(bmi <= 24.99 && bmi >= 18.5){
                shape = ("NORMAL").toString()
            }else if(bmi >= 25.0 && bmi <= 34.99){
                shape = ("OVERWEIGHT").toString()
            }else{
                shape = ("OBESE").toString()
            }
            var action = BmiFragmentDirections.actionBmiFragmentToResBmiFragment2(shape.toString(),bmi.toString())
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BmiFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            BmiFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}